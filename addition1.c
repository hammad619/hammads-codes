#include<stdio.h>
int main()
{
    int num1, num2, sum;
    printf("Addition of two numbers\n");
    printf("Enter the first value: ");
    scanf("%d", &num1);
    printf("Enter the second value: ");
    scanf("%d", &num2);
    sum = num1 + num2;
    printf("%d + %d = %d\n", num1, num2, sum);
    return 0;
} 